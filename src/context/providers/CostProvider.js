import React, { useReducer } from "react";
import { CostContext, CostInitialState } from "../ResultContext";
import CostReducer from "../reducers/CostReducer";

const CostProvider = (props) => {
  const [state, dispatch] = useReducer(CostReducer, CostInitialState);
  return (
    <CostContext.Provider value={[state, dispatch]}>
      {props.children}
    </CostContext.Provider>
  );
};

export default CostProvider;
