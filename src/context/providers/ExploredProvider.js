import React, { useReducer } from "react";
import { ExploredContext, ExploredInitialState } from "../ResultContext";
import CostReducer from "../reducers/CostReducer";

const ExploredProvider = (props) => {
  const [state, dispatch] = useReducer(CostReducer, ExploredInitialState);
  return (
    <ExploredContext.Provider value={[state, dispatch]}>
      {props.children}
    </ExploredContext.Provider>
  );
};

export default ExploredProvider;
