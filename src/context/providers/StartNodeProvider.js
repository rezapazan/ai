import React, { useReducer } from "react";
import { StartNodeContext, StartInitialState } from "../StartNodeContext";
import StartNodeReducer from "../reducers/StartNodeReducer";

const StartNodeProvider = (props) => {
  const [state, dispatch] = useReducer(StartNodeReducer, StartInitialState);
  return (
    <StartNodeContext.Provider value={[state, dispatch]}>
      {props.children}
    </StartNodeContext.Provider>
  );
};

export default StartNodeProvider;
