import React, { useReducer } from "react";
import { EndNodeContext, EndInitialState } from "../EndNodeContext";
import EndNodeReducer from "../reducers/EndNodeReducer";

const EndNodeProvider = (props) => {
  const [state, dispatch] = useReducer(EndNodeReducer, EndInitialState);
  return (
    <EndNodeContext.Provider value={[state, dispatch]}>
      {props.children}
    </EndNodeContext.Provider>
  );
};

export default EndNodeProvider;
