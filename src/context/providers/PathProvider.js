import React, { useReducer } from "react";
import { PathContext, PathInitialState } from "../ResultContext";
import ResultReducer from "../reducers/ResultReducer";

const PathProvider = (props) => {
  const [state, dispatch] = useReducer(ResultReducer, PathInitialState);
  return (
    <PathContext.Provider value={[state, dispatch]}>
      {props.children}
    </PathContext.Provider>
  );
};

export default PathProvider;
