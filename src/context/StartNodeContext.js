import { createContext, useContext } from "react";

export const StartInitialState = {
  x: "Not Specified",
  y: "Not Specified",
};

export const StartNodeContext = createContext(StartInitialState);
export const useStartNode = () => useContext(StartNodeContext);
