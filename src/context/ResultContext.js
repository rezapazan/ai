import { createContext, useContext } from "react";

export const PathInitialState = [];
export const ExploredInitialState = -2;
export const CostInitialState = -2;

export const PathContext = createContext(PathInitialState);
export const usePath = () => useContext(PathContext);

export const ExploredContext = createContext(ExploredInitialState);
export const useExplored = () => useContext(ExploredContext);

export const CostContext = createContext(CostInitialState);
export const useCost = () => useContext(CostContext);
