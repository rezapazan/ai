const ResultReducer = (state, action) => {
  switch (action.type) {
    case "SET":
      return [...state, ...action.data];
    case "RESET":
      return [];
    default:
      return [...state];
  }
};

export default ResultReducer;
