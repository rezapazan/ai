const StartNodeReducer = (state, action) => {
  switch (action.type) {
    case "SET":
      return { ...state, x: action.data.x, y: action.data.y };
    case "RESET":
      return { ...state, x: "Not Specified", y: "Not Specified" };
    default:
      return { ...state };
  }
};

export default StartNodeReducer;
