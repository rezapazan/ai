const CostReducer = (state, action) => {
  switch (action.type) {
    case "SET":
      return action.data;
    case "RESET":
      return -2;
    default:
      return state;
  }
};

export default CostReducer;
