import { createContext, useContext } from "react";

export const EndInitialState = {
  x: "Not Specified",
  y: "Not Specified",
};

export const EndNodeContext = createContext(EndInitialState);
export const useEndNode = () => useContext(EndNodeContext);
