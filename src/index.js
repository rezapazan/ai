import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import StartNodeProvider from "./context/providers/StartNodeProvider";
import EndNodeProvider from "./context/providers/EndNodeProvider";
import PathProvider from "./context/providers/PathProvider";
import CostProvider from "./context/providers/CostProvider";
import ExploredProvider from "./context/providers/ExploredProvider";
import "./index.scss";
import "react-toastify/dist/ReactToastify.css";

ReactDOM.render(
  <React.StrictMode>
    <EndNodeProvider>
      <StartNodeProvider>
        <PathProvider>
          <CostProvider>
            <ExploredProvider>
              <App />
            </ExploredProvider>
          </CostProvider>
        </PathProvider>
      </StartNodeProvider>
    </EndNodeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
