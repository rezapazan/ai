import React from "react";
import ButtonPanel from "./components/ButtonPanel";
import Board from "./components/Board"
import {ToastContainer} from "react-toastify";

function App() {
    return <React.Fragment>
        <Board/>
        <ButtonPanel/>
        <ToastContainer
            position="bottom-center"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop
            closeOnClick
            pauseOnFocusLoss
            draggable
            pauseOnHover
        />
    </React.Fragment>;
}

export default App;
