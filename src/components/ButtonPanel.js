import React from "react";
import Button from "./Button";

const buttons = [
  {
    text: "Run BFS",
    clicked: "BFS",
  },
  {
    text: "Run IDS",
    clicked: "IDS",
  },
  {
    text: "Run A*",
    clicked: "A*",
  },
  {
    text: "Reset Board",
    clicked: "RESET",
  },
];

const ButtonPanel = () => {
  return (
    <div className="buttonLayout">
      <h2>Choose the algorithm</h2>
      <div>
        {buttons.map((button) => (
          <Button
            text={button.text}
            clicked={button.clicked}
            key={button.clicked}
          />
        ))}
      </div>
      <footer>Reza Pazan - Sahar Sarkar</footer>
    </div>
  );
};

export default ButtonPanel;
