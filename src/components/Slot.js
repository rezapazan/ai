import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { useStartNode } from "../context/StartNodeContext";
import { useEndNode } from "../context/EndNodeContext";
import { usePath, useCost, useExplored } from "../context/ResultContext";

const Slot = (props) => {
  const [startNode, startDispatch] = useStartNode();
  const [slotText, setSlotText] = useState("");
  const [isPath, setIsPath] = useState("");
  const [endNode, endDispatch] = useEndNode();
  const [path] = usePath();
  const [startColor, setStartColor] = useState("");
  const [endColor, setEndColor] = useState("");

  useEffect(() => {
    if (startNode.x === props.x && startNode.y === props.y) {
      setStartColor("startColor");
    } else {
      setStartColor("")
    }
  }, [startNode, props]);

  useEffect(() => {
    if (endNode.x === props.x && endNode.y === props.y) {
      setEndColor("endColor");
    } else {
      setEndColor("")
    }
  }, [endNode, props]);

  useEffect(() => {
    if (
      startNode.x === "Not Specified" &&
      startNode.y === "Not Specified" &&
      endNode.x === "Not Specified" &&
      endNode.y === "Not Specified"
    ) {
      setSlotText("");
    }
  }, [startNode, endNode]);

  useEffect(() => {
    if (path !== undefined && path.length !== 0) {
      for (const slot of path) {
        if (props.x === slot.x && props.y === slot.y) {
          setIsPath("path");
        }
      }
    } else {
      setIsPath("");
    }
  }, [path, props]);

  const clickHandler = () => {
    switch (props.color) {
      case "black":
        toast.warn("You chose a wall!!", {
          position: "bottom-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        break;
      case "white":
        if (
          startNode.x === "Not Specified" &&
          startNode.y === "Not Specified" &&
          endNode.x === "Not Specified" &&
          endNode.y === "Not Specified"
        ) {
          startDispatch({ type: "SET", data: { x: props.x, y: props.y } });
          setSlotText("S");
        } else if (
          startNode.x !== "Not Specified" &&
          startNode.y !== "Not Specified" &&
          endNode.x === "Not Specified" &&
          endNode.y === "Not Specified"
        ) {
          endDispatch({ type: "SET", data: { x: props.x, y: props.y } });
          setSlotText("E");
        } else if (
          startNode.x !== "Not Specified" &&
          startNode.y !== "Not Specified" &&
          endNode.x !== "Not Specified" &&
          endNode.y !== "Not Specified"
        ) {
          toast.warn("Start node and end node are chosen!!", {
            position: "bottom-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        break;
      default:
        break;
    }
  };

  return (
    <div
      className={"slot_" + props.color + " " + isPath + " " + startColor + " " + endColor}
      onClick={clickHandler}
    >
      {slotText}
    </div>
  );
};

export default Slot;
