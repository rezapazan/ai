import React from "react";
import { board1 } from "../Test/Board1";
import { board2 } from "../Test/Board2";
import { board3 } from "../Test/Board3";
import { board4 } from "../Test/Board4";
import { board5 } from "../Test/Board5";
import Slot from "./Slot";
import { useCost, useExplored } from "../context/ResultContext";

const Board = () => {
  const [cost] = useCost();
  const [explored] = useExplored();

  const renderCost = (number) => {
    let value = number === -2 ? "N/A" : number;
    return value;
  };

  const makeBoard = () => {
    return Object.keys(board1).map((row) => (
      <div key={row}>
        {board1[row].map((slot) => (
          <Slot color={slot.color} key={slot.id} x={slot.x} y={slot.y} />
        ))}
      </div>
    ));
  };

  return (
    <div className="board">
      <div>{makeBoard()}</div>
      <footer>
        <p>Path Cost: {renderCost(cost)}</p>
        <p>Explored Set: {renderCost(explored)}</p>
      </footer>
    </div>
  );
};

export default Board;
