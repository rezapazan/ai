import React, { useState, useEffect } from "react";
import { useStartNode } from "../context/StartNodeContext";
import { useEndNode } from "../context/EndNodeContext";
import axios from "../utils/axios/axios";
import { usePath, useCost, useExplored } from "../context/ResultContext";
import { board1 } from "../Test/Board1";
import { board2 } from "../Test/Board2";
import { board3 } from "../Test/Board3";
import { board4 } from "../Test/Board4";
import { board5 } from "../Test/Board5";
import { toast } from "react-toastify";

const Button = (props) => {
  const [startNode, startDispatch] = useStartNode();
  const [endNode, endDispatch] = useEndNode();
  const [, pathDispatch] = usePath();
  const [walls, setWalls] = useState([]);
  const [cost, costDispatch] = useCost();
  const [explored, exploredDispatch] = useExplored();

  useEffect(() => {
    let walls = Object.keys(board1).reduce((final, row) => {
      final.push(
        board1[row].reduce((result, slot) => {
          if (slot.color === "black") {
            result.push(slot);
          }
          return result;
        }, [])
      );
      return final;
    }, []);
    setWalls(walls);
  }, []);

  const clearSelection = () => {
    startDispatch({ type: "RESET" });
    endDispatch({ type: "RESET" });
    pathDispatch({ type: "RESET" });
    costDispatch({ type: "RESET" });
    exploredDispatch({ type: "RESET" });
  };

  const sendReq = (algorithm) => {
    if (
      startNode.x === "Not Specified" &&
      startNode.y === "Not Specified" &&
      endNode.x === "Not Specified" &&
      endNode.y === "Not Specified"
    ) {
      toast.warn("No node is chosen!!", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else if (
      startNode.x !== "Not Specified" &&
      startNode.y !== "Not Specified" &&
      endNode.x === "Not Specified" &&
      endNode.y === "Not Specified"
    ) {
      toast.warn("End node is not chosen!!", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      let url = "/" + algorithm + "/";
      axios
        .post(url, {
          start: startNode,
          end: endNode,
          walls,
        })
        .then((res) => {
          if (res.data.cost === -1) {
            toast.warn("No path was found!!", {
              position: "bottom-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          } else {
            pathDispatch({ type: "RESET" });
            pathDispatch({ type: "SET", data: res.data.path });
            costDispatch({ type: "SET", data: res.data.cost });
            exploredDispatch({ type: "SET", data: res.data.visited });
          }
        })
        .catch((error) => {
          if (error) {
            toast.warn("Something went wrong!!", {
              position: "bottom-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
        });
    }
  };

  const clickHandler = () => {
    switch (props.clicked) {
      case "BFS":
        sendReq("bfs");
        break;
      case "IDS":
        sendReq("ids");
        break;
      case "A*":
        sendReq("astar");
        break;
      case "RESET":
        clearSelection();
        break;
      default:
        console.log("BFS Started");
        break;
    }
  };

  return (
    <button className="button" onClick={clickHandler}>
      {props.text}
    </button>
  );
};

export default Button;
